#!/bin/sh

SRC="columns.c stringPlus.mli stringPlus.ml parsing_helper.mli parsing_helper.ml stringmap.mli stringmap.ml types.mli ptree.mli piptree.mli pitptree.mli  funsymbhash.mli funsymbhash.ml tree.mli tree.ml pitypes.mli param.mli param.ml parser.mli parser.ml lexer.ml pitparser.mli pitparser.ml pitlexer.ml pvqueue.mli pvqueue.ml terms.mli terms.ml termslinks.mli termslinks.ml display.mli display.ml termsEq.mli termsEq.ml reduction_helper.mli reduction_helper.ml evaluation_helper.mli evaluation_helper.ml history.mli history.ml pievent.mli pievent.ml weaksecr.mli weaksecr.ml noninterf.mli noninterf.ml database.mli database.ml selfun.mli selfun.ml rules.mli rules.ml syntax.mli syntax.ml tsyntax.mli tsyntax.ml piparser.mli piparser.ml pilexer.ml spassout.mli spassout.ml simplify.mli simplify.ml pisyntax.mli pisyntax.ml pitsyntax.mli pitsyntax.ml pitransl.mli pitransl.ml pitranslweak.mli pitranslweak.ml destructor.mli destructor.ml lemma.mli lemma.ml reduction.mli reduction.ml reduction_bipro.mli reduction_bipro.ml piauth.mli piauth.ml move_new_let.mli move_new_let.ml proswapper.mli proswapper.ml encode_queries.mli encode_queries.ml version.mli version.ml main.ml"

SRCTOTEX="stringPlus.cmx parsing_helper.cmx param.cmx piparser.cmx pilexer.cmx pitparser.cmx pitlexer.cmx fileprint.ml lexertotex.ml pitlexertotex.ml version.mli version.ml proveriftotex.ml"

SRCTOTEXC="stringPlus.cmo parsing_helper.cmo param.cmo piparser.cmo pilexer.cmo pitparser.cmo pitlexer.cmo fileprint.ml lexertotex.ml pitlexertotex.ml version.mli version.ml proveriftotex.ml"

SRCINTERACT="columns.c stringPlus.mli stringPlus.ml parsing_helper.mli parsing_helper.ml stringmap.mli stringmap.ml types.mli ptree.mli pitptree.mli funsymbhash.mli funsymbhash.ml tree.mli tree.ml pitypes.mli param.mli param.ml parser.mli parser.ml lexer.ml pitparser.mli pitparser.ml pitlexer.ml types.mli pitypes.mli pvqueue.mli pvqueue.ml terms.mli terms.ml termslinks.mli termslinks.ml display.mli display.ml termsEq.mli termsEq.ml reduction_helper.mli reduction_helper.ml evaluation_helper.mli evaluation_helper.ml simplify.mli simplify.ml history.mli history.ml pievent.mli pievent.ml database.mli database.ml pitsyntax.mli pitsyntax.ml destructor.mli destructor.ml lemma.mli lemma.ml reduction_bipro.mli reduction_bipro.ml display_interact.mli display_interact.ml menu_helper.mli menu_helper.ml reduction_interact.mli reduction_interact.ml convert_repl.mli convert_repl.ml menu_interact.mli menu_interact.ml version.mli version.ml main_interact.ml"

if uname -a | egrep -q \(Cygwin\)\|\(MINGW\)
then
    EXEC=.exe
else
    EXEC=
fi

if [ X"$1" = X-nointeract ]
then
    INTERACT=false
    shift
else
    INTERACT=true
fi

dune_build()
{
    dune build src/$2.exe --no-print-directory &&\
    if [ "$2.exe" != "$1$EXEC" ]
    then
	mv $2.exe $1$EXEC
    fi
}

ocb_build()
{
    ocamlbuild $OPTIONS -use-ocamlfind $2.$TARGET &&\
    cp _build/$2.$TARGET ../$1$EXEC &&\
    rm -f $2.$TARGET
}	    

ci_build()
{
    if [ -x ci_system/build ]
    then
	
	cd ci_system &&\
	case X$1 in
	    Xdune )
		./build dune ;;
	    X | X*native | X*profile)
		./build ;;
	    X*byte)
		./build byte ;;
	    X*debug)
		./build debug	
	esac &&\
	cp analyze$EXEC addexpectedtags$EXEC .. &&\
	cd ..

    else
	echo "ci_system not found; you might want to update the ci_system submodule"    
    fi
}

case X$1 in
    Xdune )
	echo Compilation of all ProVerif executables &&\
	dune_build proverif main &&\
	dune_build proveriftotex proveriftotex &&\
	if [ $INTERACT = true ]
	then
	    dune_build proverif_interact main_interact
	fi &&\
	ci_build "$1" &&\
	echo Build successful! &&\
	echo You can invoke ./proverif --help to display version data and usage information. ;;

    Xocb.*)
	case X$1 in
	    Xocb.native )
		TARGET="native"
		OPTIONS="";;
	    Xocb.byte )
		TARGET="byte"
		OPTIONS="";;
	    Xocb.debug )
		TARGET="byte"
		OPTIONS="-tag debug";;
	esac

	cd src &&\
	    ocb_build proverif main &&\
	    ocb_build proveriftotex proveriftotex &&\
	    if [ $INTERACT = true ]
	    then
		ocb_build proverif_interact main_interact 
	    fi
	cd ..
	ci_build "$1";;

    Xprofile | Xnative | X | Xbyte | Xdebug )
	cd src &&\
	ocamlyacc -v parser.mly &&\
	ocamllex lexer.mll &&\
	ocamlyacc -v piparser.mly &&\
	ocamllex pilexer.mll &&\
	ocamlyacc -v pitparser.mly &&\
	ocamllex pitlexer.mll &&\
	ocamllex lexertotex.mll &&\
	ocamllex pitlexertotex.mll &&\
	case X$1 in
	    Xprofile )
		ocamlopt -o ../proverif$EXEC unix.cmxa profileprim.c profile.mli profile.ml str.cmxa $SRC &&\
		ocamlopt -o ../proveriftotex$EXEC unix.cmxa $SRCTOTEX &&\
		if [ $INTERACT = true ]
		then
		    ocamlfind ocamlopt -o ../proverif_interact$EXEC -package lablgtk2 -package unix -package str -linkpkg $SRCINTERACT
		fi;;
	    Xnative | X )
		ocamlopt -I +unix -I +str -o ../proverif$EXEC unix.cmxa str.cmxa $SRC &&\
		ocamlopt -I +unix -o ../proveriftotex$EXEC unix.cmxa $SRCTOTEX &&\
		if [ $INTERACT = true ]
		then
		    ocamlfind ocamlopt -o ../proverif_interact$EXEC -package lablgtk2 -package unix -package str -linkpkg $SRCINTERACT
		fi;;
	    Xbyte )
		ocamlc -I +unix -I +str -o ../proverif$EXEC -custom unix.cma str.cma $SRC &&\
		ocamlc -I +unix -o ../proveriftotex$EXEC unix.cma $SRCTOTEXC &&\
		if [ $INTERACT = true ]
		then
		    ocamlfind ocamlc -o ../proverif_interact$EXEC -custom -package lablgtk2 -package unix -package str -linkpkg $SRCINTERACT
		fi;;
	    Xdebug )
		ocamlc -I +unix -I +str -g -o ../proverif$EXEC -custom unix.cma str.cma $SRC &&\
		ocamlc -g -I +unix -o ../proveriftotex$EXEC unix.cma $SRCTOTEXC &&\
		if [ $INTERACT = true ]
		then
		    ocamlfind ocamlc -g -o ../proverif_interact$EXEC -custom -package lablgtk2 -package unix -package str -linkpkg $SRCINTERACT
		fi
	esac
	cd ..
	ci_build "$1" ;;

    Xclean )
	rm -f proveriftotex$EXEC
	rm -f proverif$EXEC
        rm -f proverif_interact$EXEC
	rm -f analyze$EXEC
	rm -f addexpectedtags$EXEC
	rm -f src/*lexer.ml src/*lexertotex.ml src/*parser.ml src/*parser.mli src/*.cmx src/*.cmo src/*.cmi src/*.o
	rm -rf src/_build _build

	if [ -d ci_system ]
	then
	    cd ci_system
	    ./build clean
	    cd ..
	fi;;
    
    Xinstall )
	cp proverif$EXEC $2/bin/proverif$EXEC &&\
	chmod +x $2/bin/proverif$EXEC &&\
	cp proveriftotex$EXEC $2/bin/proveriftotex$EXEC &&\
	chmod +x $2/bin/proveriftotex$EXEC &&\
	mkdir -p $2/doc/proverif &&\
	mkdir -p $2/share/proverif &&\
	cp analyze$EXEC $2/doc/proverif/analyze$EXEC &&\
	chmod +x $2/doc/proverif/analyze$EXEC &&\
	cp addexpectedtags$EXEC $2/doc/proverif/addexpectedtags$EXEC &&\
	chmod +x $2/doc/proverif/addexpectedtags$EXEC &&\
	cp -r examples $2/doc/proverif &&\
	cp docs/manual-untyped.pdf README LICENSE $2/doc/proverif &&\
	cp cryptoverif.pvl cssproverif.css $2/share/proverif &&\
	cp -r emacs $2/share/proverif/emacs &&\
	if [ $INTERACT = true ]
	then
	    cp proverif_interact$EXEC $2/bin/proverif_interact$EXEC &&\
	    chmod +x $2/bin/proverif_interact$EXEC
	fi;;

    *)
	echo Unknown option
esac


