(* Proswapper: compile barriers *)

val compile_barriers : (Pitypes.t_pi_state -> unit) -> Pitypes.t_pi_state -> unit

