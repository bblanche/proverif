(* Basic weak secret example, from JLAP paper
   First version, with an attack *)

type skey.
type pkey.
type passwd.

(* public-key encryption *)

fun pk(skey): pkey.
fun penc(bitstring, pkey): bitstring.
fun pdec(bitstring, skey): bitstring.
equation forall x: bitstring, y: skey;  pdec(penc(x, pk(y)), y) = x.

(* Hash function *)

fun h(passwd): bitstring.

free c: channel.
free w: passwd [private].

weaksecret w.

process
	new s: skey;
	out(c, pk(s));
	out(c, penc(h(w), pk(s)))


(* EXPECTPV
RESULT Weak secret w is false.
0.017s (user 0.010s + system 0.007s), max rss 10160K
END *)
